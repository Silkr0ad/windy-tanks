﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ChangeVCamTarget : MonoBehaviour {
    public static void ChangeCameraTarget(CinemachineVirtualCamera followCam, Transform target) {
        followCam.m_Follow = target;
        followCam.m_LookAt = target;
    }
}
