# Project Windy Tanks

A group project given as a homework assignment in CS 583 ("3D Game Programming"); the final project.

## Media

![Showcase GIF](Media/ezgif.com-optimize.gif)


## Attributions

### Art

UI background: https://wonderfulengineering.com/wp-content/uploads/2014/01/tank-wallpapers-4.jpg and https://unsplash.com/photos/yI3weKNBRTc

Ground texture: https://opengameart.org/content/29-grounds-and-walls-and-water-1024x1024

Skybox: from http://www.custommapmakers.org/skyboxes.php by Hazel Whorley

### Music & Sound Effects

TODO: needs citation
