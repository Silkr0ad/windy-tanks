﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour {
    public int xSize = 16;
    public int zSize = 16;
    public float resolution = 1f;
    public float scale = 1f;
    public int octaves = 3;
    public float persistence = 0.5f;
    public float lacunarity = 2f;
    public float height = 1f;
    public bool update = false;
    
    [HideInInspector] public Mesh mesh;
    private Vector3[] vertices;
    private int[] triangles;
    private Vector2[] uv;

    private void Start() {
        // create a new (empty) mesh
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        // add basic terrain and save the mesh as an asset
        CreateShape();
        UpdateMesh();
    }

    private void Update() {
        if (update) {
            // move the transform to the center
            AdjustPosition();
            // add basic terrain and update the mesh
            CreateShape();
            UpdateMesh();
        }
        
        #region Scale and Lacunarity Adjustment
        // add ability to change scale and lacunarity in-game | FOR DEBUGGING PURPOSES
        if (Input.GetKey(KeyCode.LeftArrow)) {
            scale -= 40 * Time.deltaTime;
        } else if (Input.GetKey(KeyCode.RightArrow)) {
            scale += 40 * Time.deltaTime;
        } else if (Input.GetKey(KeyCode.DownArrow)) {
            lacunarity -= 1 * Time.deltaTime;
        } else if (Input.GetKey(KeyCode.UpArrow)) {
            lacunarity += 1 * Time.deltaTime;
        }
        #endregion
    }

    private void CreateShape() {
        // initialize the arrays
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];
        triangles = new int[xSize * zSize * 6];
        uv = new Vector2[vertices.Length];

        // clamp scale
        scale = Mathf.Clamp(scale, 0.001f, Mathf.Infinity);

        #region Generate the Vertices
        for (int i = 0, z = 0; z < zSize + 1; z++) {
            for (int x = 0; x < xSize + 1; x++) {
                float frequency = 1f, amplitude = 1f, y = 0f;
                for (int octave = 0; octave < octaves; octave++) {
                    y += (Mathf.PerlinNoise(x * frequency / scale, z * frequency / scale) * 2 - 1) * amplitude * height;
                    frequency *= lacunarity;
                    amplitude *= persistence;
                }
                vertices[i++] = new Vector3(x / resolution, y, z / resolution);
            }
        }
        #endregion

        #region Generate the Triangles
        int vert = 0;
        int tris = 0;
        for (int z = 0; z < zSize; z++) {
            for (int x = 0; x < xSize; x++) {
                triangles[tris + 0] = vert;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert += 1;
                tris += 6;
            }
            vert += 1;
        }
        #endregion

        #region Generate the UVs
        for (int i = 0, z = 0; z < zSize + 1; z++) {
            for (int x = 0; x < xSize + 1; x++) {
                uv[i++] = new Vector2((float)x / xSize, (float)z / zSize);
            }
        }
        #endregion
    }

    private void UpdateMesh() {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
    }

    public void AdjustPosition() {
        transform.position = new Vector3(-xSize / 2f / resolution, 0f, -zSize / 2f / resolution);
    }

    private void OnDrawGizmosSelected() {
        if (vertices == null)
            return;
        for (int i = 0; i < vertices.Length; i++) {
            Gizmos.DrawSphere(vertices[i], 0.2f);
        }
    }
}
