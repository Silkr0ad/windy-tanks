﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlightController : MonoBehaviour {
    public float orthogonalSpeed;
    public float lateralSpeed;
    public float sensitivity;
    public float smoothing;

    private Vector2 mouseLook;
    private Vector2 smoothedLook;

    private Camera cam;

    private void Awake() {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }

    void Update() {
        // set direction
        Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        mouseDelta *= sensitivity * smoothing;
        smoothedLook = Vector2.Lerp(smoothedLook, mouseDelta, 1f / smoothing);
        mouseLook += smoothedLook;
        cam.transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);

        // move
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 direction = transform.forward * ver * orthogonalSpeed + transform.right * hor * lateralSpeed;
        cam.transform.Translate(direction * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Q))
            Cursor.lockState = CursorLockMode.None;
    }
}
