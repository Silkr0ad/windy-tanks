﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshGenerator))]
public class SaveMesh : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        MeshGenerator mg = (MeshGenerator)target;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Save Mesh as Asset")) {
            AssetDatabase.DeleteAsset("Assets/Models/Terrain.asset");
            AssetDatabase.CreateAsset(mg.mesh, "Assets/Models/Terrain.asset");
            AssetDatabase.SaveAssets();
        }
        
        if (GUILayout.Button("Adjust Position")) {
            mg.AdjustPosition();
        }

        GUILayout.EndHorizontal();
    }
}
