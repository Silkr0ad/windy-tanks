﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    public float rotationSpeed = 20f;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(Input.GetAxis("Vertical") * Time.deltaTime * -rotationSpeed, Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed, 0) * transform.rotation;
        Debug.Log(Input.GetAxis("Vertical") + " | " + Input.GetAxis("Horizontal"));
    }
}
